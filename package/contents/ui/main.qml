import QtQuick 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents3
import QtQuick.Layouts 1.1

Item {
    id: root

    PlasmaCore.Dialog {
        type: PlasmaCore.Dialog.PopupMenu
        backgroundHints: PlasmaCore.Dialog.NoBackground
        location: PlasmaCore.Types.Floating
        flags: Qt.ToolTip | Qt.WindowTransparentForInput | Qt.WindowStaysOnTopHint | Qt.WindowDoesNotAcceptFocus
        visible: true
        x: 0
        y: 0

        mainItem: Rectangle {
            id: rect
            width: Math.max(1, grid.width)
            height: Math.max(1, grid.height)
            color: "transparent"
            property int sizeCounter: 0
            PlasmaCore.FrameSvgItem {
                id: frameSvg
                imagePath: "dialogs/background"
                anchors {
                    fill: parent
                }
            }
            GridLayout {
                id: grid
                rows: 1

                Repeater {
                    model: ListModel {
                        id: listModel
                    }

                    delegate: Row {
                        width: childrenRect.width
                        height: childrenRect.height
                        PlasmaComponents3.Label {
                            text: model.label
                        }
                        PlasmaComponents3.Label {
                            text: model.text
                        }
                    }
                }

                Component.onCompleted: {
                    for (var i = 0; i < 10; i++) {
                        listModel.append({'label': rect.sizeCounter.toString(), 'text': rect.sizeCounter.toString()});
                    }
                }
            }
            Timer {
                id: timer
                interval: 300
                repeat: true
                running: true
                onTriggered: {
                    rect.sizeCounter = (rect.sizeCounter + 1) % 10;
                    for (var i = 0; i < 10; i++) {
                        var text = "";
                        for (var j = 0; j < rect.sizeCounter; j++) {
                            text += rect.sizeCounter.toString();
                        }
                        listModel.set(i, {'label': rect.sizeCounter.toString(), 'text': text});
                    }
                }
            }
        }
    }
}
